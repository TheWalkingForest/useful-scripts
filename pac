#!/usr/bin/env bash

case $1 in
  it)
    paru "$2"
    ;;
  up)
    paru
    ;;
  rm)
    sudo pacman -Rs "$2"
    ;;
  rmo)
    paru -c
    ;;
  li)
    if [[ $2 == "inst" ]]; then
      pacman -Qe
    else
      pacman -Q
    fi
    ;;
  bk)
    pacman -Qqe > "$HOME"
    ;;
  sync)
    doas pacman -Syyu
    ;;
  *) 
    echo "Command \"${1}\" not recognised";;
esac
