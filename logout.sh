#!/usr/bin/env bash

case $XDG_SESSION_DESKTOP in
  i3)
    session-loggout ;;
  leftwm)
    pkill leftwm ;;
  herbstluftwm)
    herbstclient quit ;;
  *)
    echo "$XDG_SESSION_DESKTOP not supported" ;;
esac
