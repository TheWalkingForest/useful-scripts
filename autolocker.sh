#!/usr/bin/env bash

[ ! "$(pgrep xautolock)" ] && xautolock \
        -time 10 \
        -locker i3lock-fancy \
        -nowlocker i3lock-fancy \
        -detectsleep
