#!/usr/bin/env bash

MIRROR="https://repo-us.voidlinux.org/"

GREEN="$(tput setaf 4)"
YELLOW="$(tput setaf 3)"
RESET="$(tput sgr0)"

arrow="${GREEN}==>${RESET}"

if [ -z "$XDG_DATA_HOME" ]; then
    printmsg "Setting XDG_DAT_HOME to $HOME/.local/share"
    export XDG_DATA_HOME="$HOME/.local/share"
fi

if [ -z "$XDG_CONFIG_HOME" ]; then
    printmsg "Setting XDG_DAT_HOME to $HOME/.config"
    export XDG_CONFIG_HOME="$HOME/.config"
fi

addrepos() {
    echo "######################################"
    echo "########## Installing Repos ##########"
    echo "######################################"

    repos=("void-repo-nonfree" "void-repo-multilib" "void-repo-multilib-nonfree")
    for p in "${repos[@]}"; do
        printmsg "Installing $p"
        sudo xbps-install -y "$p"
    done
    sudo xbps-install -Syu
}

mirrors() {
    echo "####################################"
    echo "########## Setting Mirror ##########"
    echo "####################################"

    printf '%s Mirror Selected: %s\n' "$arrow" "$MIRROR"
    printf "Continue? [Y\\n]: "
    read -r cont
    
    if [[ $cont == "n" && $cont == "N" ]]; then
        printf "Not changing repos\n"
    else
        sudo mkdir -p /etc/xbps.d
        sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
        set -i s|"https://alpha.de.repo.voidlinux.org"|"$MIRROR"|g
        sudo xbps-install -S
    fi
}

voidsrc() {
    echo "#########################################"
    echo "########## Setting up void-src ##########"
    echo "#########################################"
    
    printmsg "cloning void-packages.git"
    git clone https://github.com/void-linux/void-packages.git "$HOME/void-packages"
    cd "$HOME/void-packages" || return
    printmsg "setting up with binary-bootstrap"
    ./xbps-src binary-bootstrap
    printmsg "allowing restricted packages"
    echo XBPS_ALLOW_RESTRICTED=yes >> "$HOME/void-packages/etc/conf"
}

installpkgs() {
    echo "#########################################"
    echo "########## Installing Packages ##########"
    echo "#########################################"

    pkgs=( \
        "base-devel" \
        "curl" "wget" "zip" "unzip" \
        "openntpd" "cronie" "libvirt" "elogind" \
        "xtools" "xdg-utils"
        "exa" "ripgrep" "bat" "ncdu" "htop" \
        "fish-shell" "starship" \
        "tmux" "vim" "neovim" "ufw" \
        "neofetch" "onefetch" \
        "python3-devel" "python2-devel" "nodejs" "npm" \
    )

    for p in "${pkgs[@]}"; do
        printmsg "Installing $p"
        sudo xbps-install -y "$p"
    done
    xcheckrestart
}

installkernels() {
    echo "############################################################"
    echo "########## Installing linux and linux lts kernels ##########"
    echo "############################################################"

    printmsg "Installing Kernels"
    sudo xbps-install -y linux linux-headers linux-lts linux-lts-headers linux-mainline linux-mainline-headers
    printmsg "Updating Grub"
    sudo update-grub
}

purgekernels() {
    echo "##########################################"
    echo "########## Removing old kernels ##########"
    echo "##########################################"

    printmsg "Removing kernels"
    sudo vkpurge rm all
    printmsg "Updating Grub"
    sudo update-grub
}

startservices() {
    echo "#######################################"
    echo "########## Starting Services ##########"
    echo "#######################################"

    services=("elogind" "libvirtd" "virtlockd" "virtlogd")

    for s in "${services[@]}"; do
        if [ -d "/etc/sv/$s" ]; then
            if ! [  -d "/var/service/$s" ]; then
                printmsg "Linking /etc/sv/$s to /var/service/$s"
                sudo ln -s "/etc/sv/$s" "/var/service/$s"
                sudo sv up "$s"
            else
                printmsg "$s already Running"
                sudo sv status "$s"
            fi
        else
            printmsg "$s does not exist"
        fi
    done
}

arrow() {
    printf '%s %s\n' "$arrow" "other string"
}

printmsg() {
    printf '%s %s\n' "$arrow" "$@"
}

main() {
    option=0
    while [ $option -ne 7 ]; do
        printf "#############################################\n"
        printf "#                                           #\n"
        printf "# This is a simple script to setup a basic  #\n"
        printf "# void system to build off of. The options  #\n"
        printf "# are meant to be run in order. Some        #\n"
        printf "# services will not exist if their packages #\n"
        printf "# are not installed first                   #\n"
        printf "#                                           #\n"
        printf "#############################################\n"
        printf "#                                           #\n"
        printf "# Menu                                      #\n"
        printf "# 1. Change mirror                          #\n"
        printf "# 2. Add multilib and non-free repos        #\n"
        printf "# 3. Install linux and linux-lts kernels    #\n"
        printf "# 4. Install packages                       #\n"
        printf "# 5. Start Services                         #\n"
        printf "# optional                                  #\n"
        printf "# 6. Set up void-src                        #\n"
        printf "# 7. Remove old kernels                     #\n"
        printf "# 8. exit                                   #\n"
        printf "#                                           #\n"
        printf "#############################################\n"
	
	printf "\n > "
        read -r option

        re='^[0-9}+$'
        if ! [[ $option =~ $re ]]; then
            case $option in
                1)
                    mirrors ;;
                2)
                    addrepos ;;
                3)
                    installkernels ;;
                4)
                    installpkgs ;;
                5)
                    startservices ;;
                6)
                    voidsrc ;;
                7)
                    purgekernels ;;
                8)
                    printf "Exiting...\n"
                    return ;;
                9)
                    arrow ;;
                *)
                    printf "Not an option, chose 1-7\n" ;;
            esac
        fi
    done
}

main
